let fs = require('fs')


const allNamesString = fs.readFileSync('names.txt', { encoding: 'utf-8' });
const nameArray = allNamesString.split('\r\n');


const allSpecesString = fs.readFileSync('subspecies.txt', { encoding: 'utf-8' });
const subspeciesArray = allSpecesString.split('\r\n');

const favouriteFoods = ['deer', 'rabbit', 'cattle', 'birds', 'boar']
const demeanors = ['friendly', 'shy', 'aggressive', 'unknown'];

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getRandomFloat(max) {
    return Math.round(Math.random() * Math.floor(max));
}

function randomOf(array) {
    return array[Math.floor(Math.random() * array.length)];
}

let tigers = []
let id = 1;
for (name of nameArray) {
    tigers.push({
        id: id,
        imageUrl: `/${id++}.jpg`,
        name: name,
        size: parseFloat((getRandomFloat(1.6) + 1.4).toFixed(2)),
        weight: getRandomInt(150) + 100,
        age: getRandomInt(18) + 2,
        favouriteFood: randomOf(favouriteFoods),
        demeanor: randomOf(demeanors),
        subSpecies: randomOf(subspeciesArray)
    })
}

const db = {tigers: tigers}
fs.writeFileSync('db.json', JSON.stringify(db, null, '\t'), {encoding: 'utf-8'})