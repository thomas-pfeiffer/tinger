const fs = require('fs');
const tigerCount = JSON.parse(fs.readFileSync('./db.json', 'utf-8')).tigers.length
console.log(`Found ${tigerCount} tigers!`)

module.exports = (req, res, next) => {
    if(req.url === '/tigers/random') {
        const id = Math.floor(Math.random() * tigerCount)
        req.url = `/tigers/${id}`
        console.log(`Rewrote to ${req.url}`)
    }
    next()
}