import React, { useState } from "react";
import { Tiger } from "./Tiger";

export function TigerDisplay(props: { tiger: Tiger }) {
  let [petCount, setPetCount] = useState(0);

  return (
    <div>
      <div className="card">
        <div className="card-image">
          <figure className="image is-4by3">
            <img src={props.tiger.imageUrl} alt="Placeholder" />
          </figure>
        </div>
        <div className="card-content">
          <div className="media">
            <div className="media-left">
              {petCount > 2 && (
                <span role="img" aria-label="heart">
                  💞
                </span>
              )}
            </div>
            <div className="media-content">
              <p className="title is-4">{props.tiger.name}</p>
            </div>
          </div>
          <footer className="card-footer">
            <button
              onClick={() => setPetCount(petCount + 1)}
              className="button is-primary card-footer-item is-small"
            >
              Streicheln
            </button>
          </footer>
        </div>
      </div>
    </div>
  );
}
