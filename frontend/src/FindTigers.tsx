import React, { useState, useEffect } from 'react';
import { Tiger } from './Tiger';
import { TigerDisplay } from './TigerDisplay';
import "./FindTigers.css";

export function FindTigers(props: { onBuyTiger: (tiger: Tiger) => void }) {
    const { tiger, next } = useRandomTiger();

    if (!tiger) {
        return <div>Loading...</div>
    }

    const buyTiger = () => {
        props.onBuyTiger(tiger);
        next();
    }

    return (
        <div className="find-tiger">
            <h1 className="title">Tiger finden</h1>
            <div className="columns">
                <button className="button is-rounded is-primary mr-4" onClick={buyTiger}>Kaufen</button>
                <button className="button is-rounded" onClick={next}>Nächster</button>
            </div>
            <div className="tiger-container">
                <TigerDisplay tiger={tiger} />
            </div>
        </div>
    );
}

function useRandomTiger() {
    let [tiger, setTiger] = useState<Tiger | undefined>(undefined);

    const loadTiger = () => {
        fetch('/tigers/random')
            .then(r => r.json().then((t: Tiger) => setTiger(t)))
    }

    useEffect(() => {
        loadTiger();
    }, []);

    return { tiger, next: loadTiger }
}