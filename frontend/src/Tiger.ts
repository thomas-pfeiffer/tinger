export interface Tiger {
    id: number;
    name: string;
    imageUrl: string;
    size: string;
    weight: number;
    age: number;
    favouriteFood: string;
    demeanor: string;
    subSpecies: string;
}
