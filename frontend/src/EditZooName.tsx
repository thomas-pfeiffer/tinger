import React, { useEffect } from "react";
import './EditZooName.css';

export function EditZooName(props: {  name: string;  onNameChange: (newName: string) => void; }) {
  useEffect(() => {
    const oldTitle = document.title;
    document.title = props.name;
    return () => {
      document.title = oldTitle;
    };
  }, [props.name]);

  return (
      <input
        className="input edit-zoo-name-input"
        type="text"
        placeholder="Zoo Name"
        onChange={(event) => props.onNameChange(event.target.value)}
      />
  );
}
