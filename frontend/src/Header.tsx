import React from 'react';
 
export interface Props {
    text: string
}
 
export function Header(props: {text: string}) {
    return (
        <div className="header">
            <h1 className="title">{props.text}</h1>
        </div>
    );
}