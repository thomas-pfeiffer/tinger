import React, { useState } from "react";
import { TigerDisplay } from "./TigerDisplay";
import { EditZooName } from "./EditZooName";
import { Tiger } from "./Tiger";
import "./Zoo.css";
import { Header } from "./Header";

export default function Zoo(props: { tigers: Tiger[] }) {
  const [zooName, setZooName] = useState("Zoo");

  return (
    <div className="zoo">
      <Header text={zooName} />
      <EditZooName name={zooName} onNameChange={setZooName} />
      <div className="tiger-grid-container">
        {props.tigers.map((tiger, i) => (
          <TigerDisplay key={i} tiger={tiger}></TigerDisplay>
        ))}
      </div>
    </div>
  );
}
