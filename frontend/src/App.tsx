import React, { useState, useEffect } from "react";
import "./App.css";
import Zoo from "./Zoo";
import { Tiger } from "./Tiger";
import { FindTigers } from "./FindTigers";

function App() {
  const [tigers, setTigers] = useState<Tiger[]>([]);

  const addTiger = (newTiger: Tiger) => setTigers(currentTigers => [...currentTigers, newTiger])

  return (
    <div className="app">
      <Zoo tigers={tigers} />
      <FindTigers onBuyTiger={addTiger} />
    </div>
  );
}

export default App;
